<?php

namespace dv\redirector\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class p extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;

    protected $request;
    protected $productFactory;
    protected $catFactory;

    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory,
                                \Magento\Framework\App\RequestInterface $request,
                                \Magento\Catalog\Model\ProductFactory $productFactory,
                                \Magento\Catalog\Model\Category $catFactory)
    {
        $this->request = $request;
        $this->productFactory = $productFactory;
        $this->_resultPageFactory = $resultPageFactory;
        $this->catFactory = $catFactory;
        parent::__construct($context);

    }

    public function execute()
    {
        $oscURL = $this->request->getParam('url');
        $new_url = $this->_redirect->getRefererUrl();

        if (preg_match("/-ocsp-([0-9]+).htm/", $oscURL, $pmatches)) {
            $osid = (int)$pmatches[1];

            //$osid = $this->request->getParam('id');
            $att = 'osc_product_id';

            $product = $this->productFactory->create();
            $p = $product->loadByAttribute($att, $osid);
            //print_r($p->getProductUrl());

            if ($p != null) {
                $new_url = $p->getProductUrl();
                //echo $new_url;
            } else {

            }
        }

        $resultPage = $this->_resultPageFactory->create();

        if($new_url){
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($new_url);
            return $resultRedirect;
        }
    }
}