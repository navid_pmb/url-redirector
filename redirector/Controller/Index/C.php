<?php

namespace dv\redirector\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class C extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;

    protected $request;
    protected $productFactory;
    protected $catFactory;

    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory,
                                \Magento\Framework\App\RequestInterface $request,
                                \Magento\Catalog\Model\ProductFactory $productFactory,
                                \Magento\Catalog\Model\Category $catFactory)
    {
        $this->request = $request;
        $this->productFactory = $productFactory;
        $this->_resultPageFactory = $resultPageFactory;
        $this->catFactory = $catFactory;
        parent::__construct($context);

    }

    public function execute()
    {
        $oscURL = $this->request->getParam('url');
        $new_url = $this->_redirect->getRefererUrl();
        if (preg_match("/^(.*?)\-ocsc-([\d|_]+)\.htm/", $oscURL, $cmatches)) {

            $catTitle = $cmatches[1];
            $oscCatId = $cmatches[2];
            $category = $this->catFactory->loadByAttribute('oscID', $oscCatId);
            echo $oscCatId;
            if ($category) {
                // if we have a good category, no need to continue loop
                $new_url = $category->getUrl();

                echo $new_url;
            }
        }
        $resultPage = $this->_resultPageFactory->create();
        if($new_url){
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($new_url);
            return $resultRedirect;
        }

    }
}